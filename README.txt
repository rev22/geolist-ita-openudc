Script to generate a database of earth coordinates of Italian cities
and municipalities, to be used by OpenUDC project.


Copyright (c) 2012 Michele Bini

This program is free software: you can redistribute it and/or modify
it under the terms of the version 3 of the GNU General Public License
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


The script requires the database IT.zip from geonames.org; which is
avalable under the Creative Commons Attribution 3.0 License
